import calculator.Logic
import calculator.model.Recipe
import calculator.model.RecipeBody
import calculator.model.RecipeDetailsResponse
import calculator.renderer.DefaultRenderer
import calculator.renderer.Renderer
import calculator.service.InMemoryService
import calculator.service.Service
import io.ktor.http.*
import io.ktor.http.ContentType.Application
import io.ktor.http.HttpHeaders.Accept
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.AuthenticationStrategy.*
import io.ktor.server.engine.*
import io.ktor.server.html.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.calllogging.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.html.html
import kotlinx.html.stream.createHTML
import java.io.InputStream
import java.net.InetAddress.getLoopbackAddress
import java.util.*

private val resourcesWhitelist = setOf("actions.js", "style.css")

private fun readResource(name: String): ByteArray {
    return Logic::class.java.classLoader.getResourceAsStream(name)!!.use(InputStream::readAllBytes)
}

fun main(args: Array<String>) {
    val tokenToFile = args.associate { arg ->
        val (token, file) = arg.split(":").also { require(it.size == 2) }
        token to file
    }
    require(tokenToFile.isNotEmpty())

    val service: Service = InMemoryService()
    val renderer: Renderer = DefaultRenderer()

    fun RoutingContext.file() = call.principal<UserIdPrincipal>()?.name!!

    embeddedServer(Netty, port = 8080, host = getLoopbackAddress().hostAddress) {
        install(CallLogging)

        install(Authentication) {
            bearer("token-auth") {
                authenticate { tokenCredential ->
                    tokenToFile[tokenCredential.token]?.let(::UserIdPrincipal)
                }
            }
        }

        install(ContentNegotiation) {
            json()
        }

        routing {
            authenticate("token-auth", strategy = Required) {
                route("/recipe") {
                    get {
                        call.request.queryParameters["id"]
                            ?.let(UUID::fromString)
                            ?.let { service.recipe(file(), it) }
                            ?.let { (recipe, flowIn, flowOut, usage) ->
                                val html = createHTML().html { renderer.renderRecipeDetails(this, flowIn, flowOut, usage) }
                                call.respond(RecipeDetailsResponse(recipe, html))
                            }
                            ?: run { call.respond(service.recipes(file())) }
                    }

                    post {
                        val recipe = call.receive<RecipeBody>()
                        service.addRecipe(file(), recipe)
                        call.respond(OK)
                    }

                    delete {
                        val recipe = call.receive<Recipe>()
                        service.deleteRecipe(file(), recipe.id)
                        call.respond(OK)
                    }
                }

                get("/balance") {
                    val result = service.balance(file())
                    when (call.request.header(Accept)?.let(ContentType::parse)) {
                        Application.Json -> call.respond(result)
                        else -> call.respondHtml(OK) {
                            renderer.renderContent(this, result)
                        }
                    }
                }
            }

            get("/main") {
                call.respondHtml(OK) {
                    renderer.renderLayout(this)
                }
            }

            get("static/{path}") {
                val name = call.parameters["path"] ?: throw IllegalArgumentException()
                if (name !in resourcesWhitelist) {
                    call.respond(NotFound)
                } else {
                    call.respondBytes(readResource(name))
                }
            }
        }
    }.start(wait = true)
}