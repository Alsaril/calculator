package calculator.renderer

import calculator.model.BalanceResult
import kotlinx.html.HTML

interface Renderer {

    fun renderLayout(html: HTML)
    fun renderContent(html: HTML, balanceResult: BalanceResult)
    fun renderRecipeDetails(
        html: HTML,
        flowIn: Map<String, Double>,
        flowOut: Map<String, Double>,
        usage: Map<String, Map<String, Pair<Double, Double>>>,
    )
}