package calculator.renderer

import calculator.model.BalanceResult
import calculator.model.dummyRecipe
import calculator.utils.prettyJson
import kotlinx.html.*
import kotlinx.html.ScriptType.textJavaScript
import kotlinx.serialization.encodeToString

class DefaultRenderer : Renderer {
    override fun renderLayout(html: HTML) {
        html.renderLayoutInternal()
    }

    private fun HTML.renderLayoutInternal() {
        head {
            link(rel = "stylesheet", href = "static/style.css")
            link(rel = "stylesheet", href = "https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic")
            link(rel = "stylesheet", href = "https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css")
            link(rel = "stylesheet", href = "https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css")
            script(type = textJavaScript, src = "static/actions.js") {}
        }

        body {
            div(classes = "main") {
                div(classes = "left-column") {
                    fieldSet {
                        label {
                            htmlFor = "token"
                            text("Token")
                        }
                        input {
                            id = "token"
                        }
                        label {
                            htmlFor = "recipe"
                            text("Recipe")
                        }
                        textArea(rows = "20") {
                            id = "recipe"
                            text(prettyJson.encodeToString(dummyRecipe()))
                        }
                        div {
                            button(classes = "button") {
                                onClick = "sendRecipe()"
                                text("Update")
                            }
                            button(classes = "button") {
                                onClick = "deleteRecipe()"
                                text("Delete")
                            }
                            button(classes = "button") {
                                onClick = "update()"
                                text("Reload")
                            }
                        }
                    }
                }
                div(classes = "center-column") {
                    id = "content"
                }
                div(classes = "right-column") {
                    id = "details"
                }
            }
        }
    }

    override fun renderContent(html: HTML, balanceResult: BalanceResult) {
        html.renderContentInternal(balanceResult)
    }

    override fun renderRecipeDetails(
        html: HTML,
        flowIn: Map<String, Double>,
        flowOut: Map<String, Double>,
        usage: Map<String, Map<String, Pair<Double, Double>>>,
    ) {
        html.renderRecipeDetailsInternal(flowIn, flowOut, usage)
    }

    private fun HTML.renderContentInternal(balanceResult: BalanceResult) {
        val rows = balanceResult.weightedRecipes
        body {
            balanceResult.comment?.let {
                div(classes = "warn") {
                    h1 {
                        text("Balance has failed")
                    }
                    text("It's likely that you have concurrent recipes such as A -> C & B -> C, where A and B are not related.")
                    br()
                    text("The raw input has been returned. Please remove this pattern manually, since constraints have not been implemented yet.")
                    br()
                    text("Exception message: $it")
                }
            }

            if (balanceResult.invalidRecipes.isNotEmpty()) {
                div(classes = "warn") {
                    h2 {
                        text("Missing recipes:")
                    }
                    ul {
                        balanceResult.invalidRecipes.forEach {
                            li {
                                onClick = "selectRecipe('${it.key.id}')"
                                text(it.key.name + it.value.joinToString(prefix = "(", postfix = ")"))
                            }
                        }
                    }
                }
            }

            h2 {
                text("Overall imbalance: " + balanceResult.weightedRecipes.filter { it.shortage > 0 }
                    .sumOf { it.shortage }.let(::round2))
            }

            table("styled-table") {
                thead {
                    tr {
                        th { text("Recipe") }
                        th { text("Shortage") }
                    }
                }
                tbody {
                    rows.forEach {
                        tr {
                            onClick = "selectRecipe('${it.recipe.id}')"
                            td("large") { text(it.recipe.name) }
                            td("large") { text(round2(it.shortage)) }
                        }
                    }
                }
            }
        }
    }

    private fun HTML.renderRecipeDetailsInternal(
        flowIn: Map<String, Double>,
        flowOut: Map<String, Double>,
        usage: Map<String, Map<String, Pair<Double, Double>>>,
    ) {
        fun TBODY.renderRow(name: String, rate: Double, percent: Double? = null, last: Boolean = false) {
            val classes = if (last) " sep" else ""
            tr {
                td(classes = classes) { text(name) }
                td(classes = classes) { percent?.let { text((it * 100).toInt().toString() + "%") } }
                td(classes = classes) { text(round2(rate)) }
                multipliers.forEach { (m, color) ->
                    val flow = rate / m / 60
                    td(classes = "multiplier-$color" + (if (flow > 1.0) " bold" else "")) {
                        text(round2(flow))
                    }
                }
            }
        }

        fun TBODY.renderHeader(text: String, label: Boolean) {
            tr {
                td("nopad" + if (label) " center" else "") {
                    colSpan = "8"
                    if (label) {
                        h3 { text(text) }
                    } else {
                        h4 { text(text) }
                    }
                }
            }
        }

        body {
            table("styled-table flow-table") {
                tbody {
                    renderHeader("Flow", label = true)
                    flowIn.asSequence().forEachIndexed { index, (name, count) ->
                        renderRow(name, count, last = index == flowIn.size - 1)
                    }
                    flowOut.forEach { (name, count) -> renderRow(name, count) }

                    if (usage.isNotEmpty()) {
                        renderHeader("Usage", label = true)
                        usage.forEach { (consumer, rates) ->
                            if (usage.size > 1) {
                                renderHeader(consumer, label = false)
                            }
                            rates.forEach { name, (rate, percent) ->
                                renderRow(name, rate, percent)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun round2(value: Double) = String.format("%.2f", value)

    private val multipliers = listOf(15 to "yellow", 30 to "red", 45 to "blue", 60 to "green", 90 to "violet")
}