package calculator.service

import calculator.Logic.safeBalance
import calculator.model.*
import calculator.utils.prettyJson
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer
import java.io.File
import java.util.*
import java.util.UUID.randomUUID

class InMemoryService : Service {
    private val recipes = mutableMapOf<String, MutableMap<UUID, Recipe>>()
    private val mapSerializer = MapSerializer(String.serializer(), MapSerializer(UUIDSerializer, Recipe.serializer()))

    init {
        loadCache()
    }

    private fun file(file: String) = recipes.computeIfAbsent(file) { _ -> mutableMapOf() }

    @Synchronized
    override fun recipes(file: String) = file(file).values.toList()

    private fun flow(products: Map<String, Double>, weight: Double): Map<String, Double> {
        return products
            .map { (name, count) -> name to weight * count }
            .sortedByDescending { it.second }
            .toMap()
    }

    override fun recipe(file: String, id: UUID): RecipeDetails {
        val balanceResult = balance(file)
        val (recipe, weight) = balanceResult.weightedRecipes.firstOrNull { it.recipe.id == id }
            ?: WeightedRecipe(file(file)[id]!!, 0.0, 0.0)

        fun calcUsage(output: String): Map<String, Pair<Double, Double>> {
            val usage = balanceResult.weightedRecipes.asSequence()
                .filter { it.recipe.inputs.containsKey(output) }
                .map {
                    val part = it.recipe.inputs[output]!! * it.weight
                    it.recipe.name to part
                }
                .sortedByDescending { it.second }
                .toList()
            val sum = usage.asSequence().map { it.second }.sum()
            val real = recipe.outputs[output]!! * weight

            return usage.asSequence().map { (name, rate) ->
                name to (rate / sum * real to rate / sum)
            }.toMap()
        }

        val usage = recipe.outputs.keys.associateWith { calcUsage(it) }.filter { it.value.isNotEmpty() }
        return RecipeDetails(recipe,
            flowIn = flow(recipe.inputs, weight),
            flowOut = flow(recipe.outputs, weight),
            usage = usage,
        )
    }

    @Synchronized
    override fun addRecipe(file: String, recipe: RecipeBody) {
        addRecipeInternal(
            file,
            Recipe(
                recipe.id ?: randomUUID(),
                recipe.name,
                recipe.inputs,
                recipe.outputs,
                recipe.multiplier,
                recipe.final
            )
        )
    }

    private fun addRecipeInternal(file: String, recipe: Recipe) {
        file(file)[recipe.id] = recipe
        storeCache()
    }

    @Synchronized
    override fun deleteRecipe(file: String, id: UUID) {
        file(file).remove(id)
        storeCache()
    }

    @Synchronized
    override fun balance(file: String): BalanceResult {
        return safeBalance(recipes(file))
    }

    private fun storeCache() {
        File(cacheName).writeText(prettyJson.encodeToString(mapSerializer, recipes))
    }

    @Suppress("UNCHECKED_CAST")
    private fun loadCache() {
        val file = File(cacheName)
        if (!file.exists()) {
            return
        }
        recipes.clear()
        file.readText()
            .let { prettyJson.decodeFromString(mapSerializer, it) as MutableMap<String, MutableMap<UUID, Recipe>> }
            .let { recipes.putAll(it) }
    }

    companion object {
        private const val cacheName = "cache.json"
    }
}