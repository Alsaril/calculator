package calculator.service

import calculator.model.BalanceResult
import calculator.model.RecipeDetails
import calculator.model.Recipe
import calculator.model.RecipeBody
import java.util.UUID

interface Service {
    fun recipes(file: String): List<Recipe>
    fun recipe(file: String, id: UUID): RecipeDetails?
    fun addRecipe(file: String, recipe: RecipeBody)
    fun deleteRecipe(file: String, id: UUID)
    fun balance(file: String): BalanceResult
}