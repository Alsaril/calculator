package calculator.model

import kotlinx.serialization.Serializable
import java.util.*

@Serializable
data class RecipeBody(
    @Serializable(with = UUIDSerializer::class)
    val id: UUID? = null,
    val name: String,
    val inputs: Map<String, Double>,
    val outputs: Map<String, Double>,
    val multiplier: Int,
    val final: Boolean,
)

@Serializable
data class Recipe(
    @Serializable(with = UUIDSerializer::class)
    val id: UUID,
    val name: String,
    val inputs: Map<String, Double>,
    val outputs: Map<String, Double>,
    val multiplier: Int,
    val final: Boolean,
)

fun dummyRecipe() = RecipeBody(
    name = "name",
    inputs = mapOf("in1" to 10.0, "in2" to 100.0),
    outputs = mapOf("out1" to 5.0, "out2" to 4.0),
    multiplier = 1,
    final = false,
)

@Serializable
data class WeightedRecipe(
    val recipe: Recipe,
    val weight: Double,
    val shortage: Double,
)