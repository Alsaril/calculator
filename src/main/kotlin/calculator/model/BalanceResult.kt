package calculator.model

import kotlinx.serialization.Serializable

@Serializable
data class BalanceResult(
    val weightedRecipes: List<WeightedRecipe>,
    val invalidRecipes: Map<Recipe, List<Pair<String, Inconsistency>>>,
    val comment: String? = null
) {
    enum class Inconsistency {
        MISSING, UNUSED
    }
}