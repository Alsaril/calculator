package calculator.model

import kotlinx.serialization.Serializable


@Serializable
data class RecipeDetails(
    val recipe: Recipe,
    val flowIn: Map<String, Double>,
    val flowOut: Map<String, Double>,
    val usage: Map<String, Map<String, Pair<Double, Double>>>,
)

@Serializable
data class RecipeDetailsResponse(
    val recipe: Recipe,
    val details: String,
)