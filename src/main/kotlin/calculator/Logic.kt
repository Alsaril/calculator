package calculator

import calculator.model.BalanceResult
import calculator.model.BalanceResult.Inconsistency.MISSING
import calculator.model.BalanceResult.Inconsistency.UNUSED
import calculator.model.Recipe
import calculator.model.WeightedRecipe
import org.apache.commons.math3.linear.Array2DRowRealMatrix
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.LUDecomposition

object Logic {
    fun safeBalance(rawRecipes: List<Recipe>) = try {
        balance(rawRecipes)
    } catch (e: Exception) {
        BalanceResult(rawRecipes.map { WeightedRecipe(it, 0.0, 0.0) }, emptyMap(), e.message)
    }

    private fun balance(rawRecipes: List<Recipe>): BalanceResult {
        val inputProducts = rawRecipes.flatMap { it.inputs.keys }.toSet()
        val outputProducts = rawRecipes.filter { !it.final }.flatMap { it.outputs.keys }.toSet()

        val (validRecipes, invalidRecipes) = rawRecipes.groupBy { recipe ->
            recipe.inputs.keys.all { it in outputProducts } && (recipe.final || recipe.outputs.keys.all { it in inputProducts })
        }.let {
            (it[true] ?: emptyList()) to (it[false] ?: emptyList())
        }

        val intermediates = validRecipes.filter { !it.final }.toList()
        val finals = validRecipes.filter { it.final }.toList()

        val products = (intermediates.flatMap { it.inputs.keys } + intermediates.flatMap { it.outputs.keys }).toSet()

        val coefficients = mutableListOf<List<Double>>()
        val constants = mutableListOf<Double>()

        val solution = if (intermediates.isNotEmpty()) {
            products.forEach { product ->
                coefficients.add(intermediates.map { recipe ->
                    recipe.outputs.getOrDefault(product, 0.0) - recipe.inputs.getOrDefault(product, 0.0)
                })

                constants.add(finals.sumOf { it.inputs[product] ?: 0.0 })
            }

            val solver =
                LUDecomposition(Array2DRowRealMatrix(coefficients.map { it.toDoubleArray() }.toTypedArray())).solver
            solver.solve(ArrayRealVector(constants.toDoubleArray())).toArray().map { if (it == -0.0) 0.0 else it }
                .toList()
        } else {
            emptyList()
        }

        val weightedRecipes = (intermediates zip solution) + finals.map { it to 1.0 }
        val result = weightedRecipes.map { (recipe, weight) ->
            WeightedRecipe(recipe, weight, weight - recipe.multiplier)
        }.sortedByDescending { it.shortage }

        return BalanceResult(
            result,
            invalidRecipes.associateWith { recipe ->
                val missingProducts =
                    recipe.inputs.map { it.key }.filter { it !in outputProducts }.map { it to MISSING }
                val unusedProducts = recipe.outputs.map { it.key }.filter { it !in inputProducts }.map { it to UNUSED }

                missingProducts + if (!recipe.final) unusedProducts else emptyList()
            }
        )
    }
}