function token() {
    return document.getElementById("token").value
}

function getRecipe() {
    return document.getElementById("recipe").value
}

function setRecipe(value) {
    return document.getElementById("recipe").value = value
}

function setDetails(value) {
    return document.getElementById("details").innerHTML = value
}

function setContent(value) {
    return document.getElementById("content").innerHTML = value
}

async function showError(response) {
    var text = await response.text()
    var details = text ? (": " + text) : ""
    alert(response.status + " " + response.statusText + details)
}

async function request(url, method, body, callback) {
    var headers = { "Authorization": "Bearer " + token() }
    var spec = { method: method, headers: headers }
    if (body) {
        headers["Content-Type"] = "application/json"
        spec["body"] = body
    }

    var response = await fetch(url, spec)

    if (response.status != 200) {
        showError(response)
        return
    }
    
    callback(response)
}

async function selectRecipe(id) {
    request("/recipe?id=" + id, "GET", null, async (response) => {
        var json = await response.json()
        setRecipe(JSON.stringify(json.recipe, null, 2))
        setDetails(json.details)
    })
}

async function sendRecipe() {
    request("/recipe", "POST", getRecipe(), (response) => {
        update()
    })
}

async function deleteRecipe() {
    request("/recipe", "DELETE", getRecipe(), (response) => {
        update()
    })
}

async function update() {
    request("/balance", "GET", null, async (response) => {
        var html = await response.text()
        setContent(html)
    })
}